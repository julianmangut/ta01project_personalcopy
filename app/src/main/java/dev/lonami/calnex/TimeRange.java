package dev.lonami.calnex;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import java.util.Locale;

public class TimeRange implements Parcelable {
    public final int startHour;
    public final int startMinute;

    public final int endHour;
    public final int endMinute;

    public TimeRange(int startHour, int startMinute, int endHour, int endMinute) {
        this.startHour = startHour;
        this.startMinute = startMinute;
        this.endHour = endHour;
        this.endMinute = endMinute;
    }

    private TimeRange(Parcel in) {
        this.startHour = in.readInt();
        this.startMinute = in.readInt();
        this.endHour = in.readInt();
        this.endMinute = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(startHour);
        dest.writeInt(startMinute);
        dest.writeInt(endHour);
        dest.writeInt(endMinute);
    }

    public static final Parcelable.Creator<TimeRange> CREATOR = new Parcelable.Creator<TimeRange>() {
        public TimeRange createFromParcel(Parcel in) {
            return new TimeRange(in);
        }

        public TimeRange[] newArray(int size) {
            return new TimeRange[size];
        }
    };

    @NonNull
    @Override
    public String toString() {
        return String.format(Locale.ENGLISH, "%d:%d - %d:%d", startHour, startMinute, endHour, endMinute);
    }
}
